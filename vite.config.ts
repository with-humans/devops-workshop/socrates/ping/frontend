import { pinoHttp } from 'pino-http';
import logger from './logger.js';
import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vitest/config';
import express from 'express';
import { loadEnv, type PluginOption, type ViteDevServer } from 'vite';

import InitSuperTokens from "./server.supertokens";
import serverUserInfo from "./server.userinfo.stub.js";

import {
	middleware as supertokensMiddleware,
	errorHandler as supertokensErrorHandler
} from 'supertokens-node/framework/express/index.js';

const loggerPlugin: PluginOption = {
	name: "logger",
	configureServer: server => {
		server.middlewares.use(pinoHttp({ logger }));
	}
}

const userInfo: PluginOption = {
	name: "userinfo",
	configureServer: server => {
		const app = express();
		app.use(serverUserInfo());
		server.middlewares.use(app);
	}
}



export default defineConfig(({ mode }) => {
	console.log({mode})
	let superTokensPlugin: PluginOption | undefined = undefined;
	const env = loadEnv(mode, process.cwd(), '')
	if (env.PUBLIC_SUPERTOKENS_URL) {
		console.log("Including Supertokens");
		InitSuperTokens(env);
		const app = express();
		app.use(supertokensMiddleware());


		superTokensPlugin = {
			name: 'supertokens',
			configureServer: async (server: ViteDevServer) => {
				server.middlewares.use(app)
				return () => {
					const postApp = express();
					postApp.use(supertokensErrorHandler());
					server.middlewares.use(postApp)
				}
			}
		}
	}

	return {
		plugins: [loggerPlugin, userInfo, superTokensPlugin, sveltekit()].filter(Boolean),
		test: {
			include: ['src/**/*.{test,spec}.{js,ts}'],
			environment: "jsdom",
			setupFiles: "vitest.setup.ts",
			alias: [{ find: /^svelte$/, replacement: 'svelte/internal' }],
			env
		}
	}
});
