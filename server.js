import logger from './logger.js';
import { pinoHttp } from 'pino-http';
import { handler } from './build/handler.js';
import express from 'express';

import cors from 'cors';
import supertokens from 'supertokens-node';
import {
	middleware as supertokensMiddleware,
	errorHandler as supertokensErrorHandler
} from 'supertokens-node/framework/express/index.js';
import { verifySession } from 'supertokens-node/recipe/session/framework/express/index.js';
import InitSuperTokens from './server.supertokens.js';
import serverUserinfo from './server.userinfo.js';

const app = express();
app.use(pinoHttp({ logger }));

InitSuperTokens(process.env);

app.use(
	cors({
		origin: process.env['PUBLIC_PING_FRONTEND_WEBSITE_DOMAIN'],
		allowedHeaders: ['content-type', ...supertokens.getAllCORSHeaders()],
		credentials: true
	})
);
app.use(supertokensMiddleware());
app.use(serverUserinfo());

app.get('/demo', verifySession(), (req, res) => {
	res.send(200, 'hallo');
});

app.get('/healthz', (_, res) => {
	res.send('OK');
});
app.use(handler);

app.use(supertokensErrorHandler());

const server = app.listen(process.env.PORT || 3000, () => {
	logger.info('listening on port ' + server.address().port);
});
