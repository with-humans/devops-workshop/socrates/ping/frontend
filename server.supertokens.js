import logger from './logger.js';
import supertokens from 'supertokens-node';
import Session from 'supertokens-node/recipe/session/index.js';
import ThirdParty from 'supertokens-node/recipe/thirdparty/index.js';
import UserMetadata from 'supertokens-node/recipe/usermetadata/index.js';

export default (env) => {
	logger.info({ env }, 'Loading Supertokens');
	const getEnvOrThrow = (key) => {
		if (!env[key]) throw 'Missing environment variable: ' + key;
		return env[key];
	};

	supertokens.init({
		framework: 'express',
		supertokens: {
			connectionURI: getEnvOrThrow('PUBLIC_SUPERTOKENS_URL'),
			apiKey: getEnvOrThrow('PRIVATE_SUPERTOKENS_API_KEY')
		},
		appInfo: {
			// learn more about this on https://supertokens.com/docs/thirdpartyemailpassword/appinfo
			appName: 'ping-frontend',
			apiDomain: getEnvOrThrow('PUBLIC_PING_FRONTEND_SUPERTOKENS_API_DOMAIN'),
			websiteDomain: getEnvOrThrow('PUBLIC_PING_FRONTEND_WEBSITE_DOMAIN'),
			apiBasePath: '/auth',
			websiteBasePath: '/auth'
		},
		recipeList: [
			UserMetadata.init(),
			ThirdParty.init({
				override: {
					functions: (originalImplementation) => {
						return {
							...originalImplementation,
							// override the thirdparty sign in / up API
							signInUp: async function (input) {
								// TODO: Some pre sign in / up logic

								let response = await originalImplementation.signInUp(input);

								if (response.status === 'OK') {
									// This is the response from the OAuth tokens provided by the third party provider
									let accessToken = response.oAuthTokens['access_token'];
									// other tokens like the refresh_token or id_token are also
									// available in the oAuthTokens object.
									if (response.user.thirdParty?.id === 'gitlab') {
										logger.debug(
											await UserMetadata.updateUserMetadata(response.user.id, {
												email: response.user.email,
												avatar_url: response.rawUserInfoFromProvider?.fromUserInfoAPI?.picture,
												preferred_username:
													response.rawUserInfoFromProvider?.fromUserInfoAPI?.preferred_username
											}),
											'New UserMetaData'
										);
									} else if (response.user.thirdParty?.id === 'github') {
										logger.debug(
											await UserMetadata.updateUserMetadata(response.user.id, {
												email: response.user.email,
												avatar_url:
													response.rawUserInfoFromProvider?.fromUserInfoAPI?.user.avatar_url,
												preferred_username:
													response.rawUserInfoFromProvider?.fromUserInfoAPI?.user.login
											}),
											'New UserMetaData'
										);
									} else {
										logger.debug(response, 'SignInUp Response');
									}
								}

								return response;
							}
						};
					}
				},
				signInAndUpFeature: {
					providers: [
						{
							config: {
								thirdPartyId: 'github',
								clients: [
									{
										clientId: env['PRIVATE_GITHUB_CLIENT_ID'],
										clientSecret: env['PRIVATE_GITHUB_CLIENT_SECRET']
									}
								]
							}
						},
						{
							config: {
								thirdPartyId: 'gitlab',
								clients: [
									{
										clientId: env['PRIVATE_GITLAB_CLIENT_ID'],
										clientSecret: env['PRIVATE_GITLAB_CLIENT_SECRET']
									}
								]
							}
						}
					]
				}
			}),
			Session.init() // initializes session features
		]
	});
};
