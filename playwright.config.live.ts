import type { PlaywrightTestConfig } from '@playwright/test';
import fs from "fs";

const playwrightenv = JSON.parse(fs.readFileSync(process.cwd() + "/.env.playwright.json").toString());

console.log("Launching with env", { env: playwrightenv });

const config: PlaywrightTestConfig = {
	webServer: {
		command: 'npm run dev',
		timeout: 5 * 60 * 1000,
		port: 5173,
		env: playwrightenv
	},
	globalSetup: './tests/globalSetup.ts',
	use: {
		permissions: ['geolocation'],
	},
	testDir: 'tests',
	testMatch: /(.+\.)?(test|spec)\.[jt]s/
};

export default config;
