FROM node:20-alpine AS builder
WORKDIR /app
ENV CI=1
COPY package.json package-lock.json ./
RUN npm ci
COPY src/theme/_smui-theme.scss ./src/theme/
RUN mkdir static && npm run smui-theme
COPY . .
RUN npm run test:unit
RUN npm run build

FROM node:20-alpine
RUN apk add --no-cache tini

WORKDIR /app
COPY --from=builder /app/package.json /app/package-lock.json ./
RUN npm ci --omit=dev
COPY --from=builder /app/build ./build
COPY --from=builder /app/server.js /app/server.*.js /app/logger.js ./
ENTRYPOINT ["/sbin/tini", "--"]
CMD ["node", "server.js"]
