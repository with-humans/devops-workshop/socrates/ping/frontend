import { devices, expect, type Page, test } from "@playwright/test";

test.beforeEach(async ({ page }) => {
  await page.route("/userinfo", (route) => {
    route.fulfill({
      status: 401,
    });
  });
  await page.route("/auth/**/*", (route) => {
    route.fulfill({
      status: 401,
    });
  });
});

test.describe("Schedule", () => {
  test("should display currently running sessions", async ({ page }) => {
    // Pick the new/fake "now" for you test pages.
    const fakeNow = new Date("2023-08-24T08:00:00.007Z").valueOf();

    // Update the Date accordingly in your test pages
    await page.addInitScript(`{
		  // Extend Date constructor to default to fakeNow
		  Date = class extends Date {
			constructor(...args) {
			  if (args.length === 0) {
				super(${fakeNow});
			  } else {
				super(...args);
			  }
			}
		  }
		  // Override Date.now() to start from fakeNow
		  const __DateNowOffset = ${fakeNow} - Date.now();
		  const __DateNow = Date.now;
		  Date.now = () => __DateNow() + __DateNowOffset;
		}`);

    await page.goto("/");
    await expect(
      page.getByRole("tooltip", { name: "Restaurant Outdoor Space" }),
    ).toBeVisible();

    await page.getByRole("button", { name: "Schedule" }).click();

    await expect(page.getByRole("heading", { name: "Schedule" }))
      .toBeInViewport();
    await expect(page.getByTestId("schedule")).toContainText("Coffee Break");
  });

  test("should highlight the room on the map if clicked on", async ({ page }) => {
    const fakeNow = new Date("2023-08-24T08:00:00.007Z").valueOf();
    await givenTimeIs(fakeNow, page);

    await page.goto("/");
    await expect(
      page.getByRole("tooltip", { name: "Restaurant Outdoor Space" }),
    ).toBeVisible();

    await page.getByRole("button", { name: "Schedule" }).click();

    await page.getByTestId("schedule").getByLabel("Pietzmoor Lounge").click();
    const target = await page.locator("#map").getByTitle("highlighted room");
    await expect(target).toBeVisible();

    const tooltip = await page.getByRole("tooltip", {
      name: "Pietzmoor Lounge",
    });
    const tooltipId = await tooltip.getAttribute("id");

    const room = await page.locator(
      'path.leaflet-interactive[aria-describedby="' + tooltipId + '"]',
    );
    const roomBoundingBox = (await room.boundingBox())!;
    const targetBoundingBox = (await target.boundingBox())!;

    expect(roomBoundingBox).not.toBeNull();
    expect(targetBoundingBox).not.toBeNull();

    expect(isOverlapping2D(roomBoundingBox, targetBoundingBox)).toBe(true);

    await expect(page.getByRole("heading", { name: "Schedule" })).not
      .toBeInViewport();
  });

  test("should only highlight the last room someone clicked on", async ({ page }) => {
    const fakeNow = new Date("2023-08-24T08:00:00.007Z").valueOf();
    await givenTimeIs(fakeNow, page);

    await page.goto("/");
    await expect(
      page.getByRole("tooltip", { name: "Restaurant Outdoor Space" }),
    ).toBeVisible();

    await page.getByRole("button", { name: "Schedule" }).click();

    await page.getByTestId("schedule").getByLabel("Pietzmoor Lounge").click();

    await page.getByRole("button", { name: "Schedule" }).click();

    await page.getByTestId("schedule").getByLabel("Restaurant").click();

    const target = await page.locator("#map").getByTitle("highlighted room");
    await expect(await target.all()).toHaveLength(1);
  })

  test("should move the room into the viewport if it is outside", async ({ page }) => {
    const fakeNow = new Date("2023-08-24T08:00:00.007Z").valueOf();
    await givenTimeIs(fakeNow, page);

    await page.goto("/");
    await expect(
      page.getByRole("tooltip", { name: "Restaurant Outdoor Space" }),
    ).toBeVisible();

    const mapBoundingBox = (await page.locator("#map").boundingBox())!;

    for (let i = 0; i < 5; i++) {
      await page.locator("#map").hover({
        position: { x: 10, y: mapBoundingBox.height - 10 },
      });
      await page.mouse.down();
      await page.locator("#map").hover({
        position: { x: mapBoundingBox?.width - 10, y: 10 },
      });
      await page.mouse.up();
    }

    expect(
      isOverlapping2D(
        mapBoundingBox,
        (await page.getByRole("tooltip", { name: "Restaurant Outdoor Space" })
          .boundingBox())!,
      ),
    ).toBe(false);

    await page.getByRole("button", { name: "Schedule" }).click();

	// Need to unmock time so pan animation in leaflet works
    await unmockTime(page);
    await page.getByTestId("schedule").getByLabel("Pietzmoor Lounge").click();

    await expect(page.locator("#map").getByTitle("highlighted room"))
      .toBeInViewport();
  });
});

// From Stackoverflow: https://stackoverflow.com/questions/20925818/algorithm-to-check-if-two-boxes-overlap
const isOverlapping1D = (
  xmin1: number,
  xmax1: number,
  xmin2: number,
  xmax2: number,
) => xmax1 >= xmin2 && xmax2 >= xmin1;
const isOverlapping2D = (
  box1: {
    x: number;
    y: number;
    width: number;
    height: number;
  },
  box2: {
    x: number;
    y: number;
    width: number;
    height: number;
  },
) =>
  isOverlapping1D(
    box1.x,
    box1.x + box1.width,
    box2.x,
    box2.x + box2.width,
  ) &&
  isOverlapping1D(
    box1.y,
    box1.y + box1.height,
    box2.y,
    box2.y + box2.height,
  );

async function givenTimeIs(fakeNow: number, page: Page) {
  await page.addInitScript(`{
		  Date.doMock = true;
		  // Extend Date constructor to default to fakeNow
		  Date = class extends Date {
			constructor(...args) {
			  if (args.length === 0 && Date.doMock) {
				super(${fakeNow});
			  } else {
				super(...args);
			  }
			}
		  }
		  // Override Date.now() to start from fakeNow
		  const __DateNowOffset = ${fakeNow} - Date.now();
		  const __DateNow = Date.now;
		  Date.now = () => Date.doMock ? __DateNow() + __DateNowOffset : __DateNow();
		}`);
}
async function unmockTime(page: Page) {
  await page.evaluate(`Date.doMock = false;`);
}
