import type { FullConfig } from '@playwright/test';

import { createServer } from '@stoplight/prism-http-server';
import { getHttpOperationsFromSpec } from '@stoplight/prism-cli/dist/operations.js';
import { createLogger } from '@stoplight/prism-core';

async function createPrismServer() {
  const operations = await getHttpOperationsFromSpec('konopas.openapi.yaml');

  const server = createServer(operations, {
    components: {
      logger: createLogger('TestLogger'),
    },
    cors: true,
    config: {
      upstreamProxy: undefined,
      checkSecurity: true,
      validateRequest: true,
      validateResponse: true,
      mock: { dynamic: false },
      isProxy: false,
      errors: false,
    },
  });
  await server.listen(12345);

  return server.close.bind(server);
}

async function globalSetup(config: FullConfig) {
  const teardownFunction = await createPrismServer();
  return teardownFunction;
}

export default globalSetup;
