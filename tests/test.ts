import { expect, test, devices } from '@playwright/test';

test.beforeEach(async ({ page }) => {
	await page.route('/userinfo', async (route) => {
		route.fulfill({
			status: 401
		});
	});
	await page.route('/auth/**/*', async (route) => {
		route.fulfill({
			status: 401
		});
	});
});

test.describe('Map', () => {
	test('Map starts in Soltau where you can see the Restaurant Outdoor Space', async ({ page }) => {
		await page.goto('/');
		await expect(page.getByRole('tooltip', { name: 'Restaurant Outdoor Space' })).toBeVisible();
	});

	// Typescript type for an array that only allows values that are keys of the variable devices
	type Device = keyof typeof devices;

	const selectedDevices: Device[] = ['Desktop Chrome', 'iPhone 12 Mini', 'iPhone 12 Pro Max'];

	selectedDevices.forEach((device) => {
		test(`All of Hotel Park Soltau is visible at the start on ${device}`, async ({ page }) => {
			await page.setViewportSize(devices[device].viewport);
			await page.goto('/');
			await expect(
				page.getByRole('tooltip', { name: 'Restaurant Outdoor Space' })
			).toBeInViewport();
			await expect(page.getByRole('tooltip', { name: 'Entrance Benches' })).toBeInViewport();
			await expect(page.getByRole('tooltip', { name: 'Dortmund' })).toBeInViewport();
		});
	});

	test('Clicking on the geolocate button shows the position of the user', async ({
		page,
		context
	}) => {
		const personInTheTentWithHighAccuracy = {
			latitude: 53.00211,
			longitude: 9.86008,
			accuracy: 1
		};
		context.setGeolocation(personInTheTentWithHighAccuracy);
		await page.goto('/');

		await expect(page.getByRole('tooltip', { name: 'Entrance Benches' })).toBeInViewport();
		await page.getByRole('button', { name: 'Geolocate' }).click();

		await expect(page.locator('css=.leaflet-control-locate-location')).toBeInViewport();
	});
});

test.skip('No debug output on the console when going to the homepage', async ({ page }) => {
	const messages: any[] = [];
	page.on('console', async (msg) => {
		if (msg.text().includes('vite')) return;
		messages.push({
			text: msg.text(),
			args: await Promise.all(msg.args().map(async (arg) => await arg.jsonValue())),
			type: msg.type(),
			location: msg.location()
		});
	});
	await page.goto('/');
	await expect(page.getByRole('tooltip', { name: 'Entrance Benches' })).toBeInViewport();
	expect(messages).toHaveLength(0);
});

test.describe('Login', () => {
	test('shows the not logged in message when not logged in', async ({ page }) => {
		await page.goto('/');
		await expect(page.getByRole('link', { name: 'Not logged in' })).toBeInViewport();
	});

	test.skip('auth lists github and gitlab as auth providers', async ({ page }) => {
		await page.goto('/auth');
		await expect(page.getByRole('button', { name: 'Continue with GitHub' })).toBeInViewport();
		await expect(page.getByRole('button', { name: 'Continue with GitLab' })).toBeInViewport();
	});

	test('shows the logged-in user when logged in', async ({ page }) => {
		await page.route('/userinfo', async (route) => {
			route.fulfill({
				status: 200,
				json: {
					metadata: {
						email: 'testuser@example.com',
						preferred_username: 'testuser',
						avatar_url: 'https://www.example.com/avatar.png'
					}
				}
			});
		});
		await page.goto('/');
		await expect(page.getByRole('button', { name: 'testuser' })).toBeInViewport();
	});
});
