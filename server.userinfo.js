import express from 'express';
import { verifySession } from 'supertokens-node/recipe/session/framework/express/index.js';
import UserMetadata from 'supertokens-node/recipe/usermetadata/index.js';

export default () => {
	const app = express();
	app.get('/userinfo', verifySession(), async (req, res) => {
		const metadata = await UserMetadata.getUserMetadata(req.session.getUserId());
		res.json(metadata);
	});
	return app;
};
