import type { FullConfig, PlaywrightTestConfig } from '@playwright/test';
import fs from "fs";

const playwrightenv = JSON.parse(fs.readFileSync(process.cwd() + "/.env.playwright.json").toString());

console.log("Launching with env", { env: playwrightenv });

const config: PlaywrightTestConfig = {
	webServer: {
		command: 'npm run build && npm run preview',
		timeout: 5 * 60 * 1000,
		port: 4173,
		env: playwrightenv
	},
	globalSetup: './tests/globalSetup.ts',
	use: {
		permissions: ['geolocation']
	},
	testDir: 'tests',
	testMatch: /(.+\.)?(test|spec)\.[jt]s/
};

export default config;
