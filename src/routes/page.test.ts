import { getByLabelText, getByText, render, screen, waitFor } from '@testing-library/svelte';
import { tick } from 'svelte';
import { describe, expect, it } from 'vitest';
import { InMemoryProvider } from '../lib/OpenFeatureInMemoryProvider';
import { initFeatureToggles } from '../lib/featureToggles';

import Page from './+page.svelte';
import { Event, Schedule } from '$lib/Schedule/domain/ScheduleDomain';
import { Duration, ZonedDateTime } from '@js-joda/core';

describe('Main Page', () => {
	it('renders', async () => {
		let target: HTMLElement;
		target = document.createElement('div');
		new Page({ target: target, props: { data: { schedule: undefined } } });
	});

	describe('login', () => {
		it('should be enabled when feature toggle is on', async () => {
			initFeatureToggles(
				new InMemoryProvider({
					'enable-user-management': true
				})
			);
			let target: HTMLElement;
			target = document.createElement('div');

			render(Page, { target, props: { data: { schedule: undefined } } });

			expect(target).toHaveTextContent('Not logged in');
		});

		it('should be hidden when feature toggle is off', async () => {
			initFeatureToggles(
				new InMemoryProvider({
					'enable-user-management': false
				})
			);
			let target: HTMLElement;
			target = document.createElement('div');

			render(Page, { target, props: { data: { schedule: undefined } } });

			expect(target).not.toHaveTextContent('Not logged in');
		});

		it('should update when the feature toggle changes', async () => {
			const provider = new InMemoryProvider({
				'enable-user-management': false
			});
			initFeatureToggles(provider);
			let target: HTMLElement;
			target = document.createElement('div');

			render(Page, { target, props: { data: { schedule: undefined } } });

			expect(target).not.toHaveTextContent('Not logged in');

			provider.replaceConfiguration({
				'enable-user-management': true
			});

			await tick();

			expect(target).toHaveTextContent('Not logged in');
		});

		it('should close the schedule when clicking on a room that exists on the map', async () => {
			const provider = new InMemoryProvider({
				'enable-schedule': true
			});
			initFeatureToggles(provider);

			const schedule = new Schedule([
				Event.of(
					'Coffee Break',
					'Pietzmoor Lounge',
					ZonedDateTime.now().minusMinutes(20),
					Duration.ofMinutes(50)
				)
			]);

			const { component } = render(Page, {
				data: { schedule },
				tryHighlightRoom: (_: string) => {
					return true;
				}
			});

			const scheduleDrawerAside = screen.getByTestId('schedule-aside');
			expect(scheduleDrawerAside).not.toHaveClass('mdc-drawer--open');

			screen.getByRole('button', { name: 'Schedule' }).click();

			await waitFor(async () => {
				await tick();
				expect(scheduleDrawerAside).toHaveClass('mdc-drawer--open');
				expect(scheduleDrawerAside).not.toHaveClass('mdc-drawer--opening');
			});

			const scheduleDrawer = screen.getByTestId('schedule');
			const pietzmoorLounge = getByText(scheduleDrawer, 'Coffee Break');
			pietzmoorLounge.click();

			await waitFor(async () => {
				await tick();
				expect(screen.queryByTestId('schedule')).toBeNull();
			});
		});
	});
});
