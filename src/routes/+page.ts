import { env } from '$env/dynamic/public';
import konopasToSchedule from '$lib/Schedule/adapter/konopas/konopasToSchedule';
import '@js-joda/timezone';
import { ZoneId } from '@js-joda/core';
import type { Schedule } from '$lib/Schedule/domain/ScheduleDomain.js';

export async function load({ fetch }): Promise<{ schedule: Schedule | undefined }> {
	if (!env['PUBLIC_KONOPAS_SCHEDULE_URL']) return { schedule: undefined };

	try {
		const scheduleRequest = await fetch(env['PUBLIC_KONOPAS_SCHEDULE_URL']);

		const schedule = konopasToSchedule(await scheduleRequest.json(), ZoneId.of('Europe/Berlin'));

		return {
			schedule
		};
	} catch (error) {
		console.error(
			{ error, konopasUrl: env['PUBLIC_KONOPAS_SCHEDULE_URL'] },
			'Error fetching konopas'
		);
		return { schedule: undefined };
	}
}
