import { initFeatureToggles } from '../lib/featureToggles';
import { FlagdWebProvider } from '@openfeature/flagd-web-provider';
import { InMemoryProvider } from '../lib/OpenFeatureInMemoryProvider';
import type { Provider } from '@openfeature/web-sdk';
import { env } from '$env/dynamic/public';
import * as SuperTokens from 'supertokens-auth-react';
import * as ThirdParty from 'supertokens-auth-react/recipe/thirdparty';
import { Github, Gitlab } from 'supertokens-auth-react/recipe/thirdparty';
import Session from 'supertokens-auth-react/recipe/session';
import { userMetaData } from '$lib/userStore';

export async function load({ url, fetch }) {
	const provider: Provider =
		env['PUBLIC_FLAGD_IN_MEMORY'] === 'true'
			? new InMemoryProvider({
					'enable-user-management': true,
					'enable-schedule': true
			  })
			: new FlagdWebProvider({
					host: url.hostname,
					port: Number(url.port) || (url.protocol.startsWith('https') ? 443 : 80),
					pathPrefix: 'api/flagd',
					tls: url.protocol.startsWith('https') || false,
					maxRetries: 1,
					maxDelay: 5
			  });
	try {
		await initFeatureToggles(provider);
	} catch (e) {
		console.error('Feature Toggles initialization failed, will use defaults only', e);
	}

	if (typeof window !== 'undefined' && !!env['PUBLIC_PING_FRONTEND_SUPERTOKENS_API_DOMAIN']) {
		SuperTokens.init({
			appInfo: {
				appName: 'ping-frontend',
				apiDomain: env['PUBLIC_PING_FRONTEND_SUPERTOKENS_API_DOMAIN'] || '',
				websiteDomain: env['PUBLIC_PING_FRONTEND_WEBSITE_DOMAIN'] || '',
				apiBasePath: '/auth',
				websiteBasePath: '/auth'
			},
			recipeList: [
				ThirdParty.init({
					signInAndUpFeature: {
						providers: [Github.init(), Gitlab.init()]
					}
				}),
				Session.init()
			]
		});
	}

	const userInfo = await fetch('/userinfo');
	if (userInfo.ok) {
		userMetaData.set({ status: 'logged_in', user: (await userInfo.json()).metadata });
	} else {
		userMetaData.set({ status: 'not_logged_in' });
	}

	return {};
}
