import logger from '../logger.js';
/** @type {import('@sveltejs/kit').HandleServerError} */
export async function handleError({ error, event }) {
	logger.error(error, 'Error', event);
}
