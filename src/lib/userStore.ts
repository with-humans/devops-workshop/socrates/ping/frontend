import { writable, type Writable } from 'svelte/store';

type UserStore =
	| {
			status: 'not_logged_in';
	  }
	| {
			status: 'logged_in';
			user: {
				email: string;
				avatar_url: string | undefined;
				preferred_username: string | undefined;
			};
	  };

export const userMetaData = writable<UserStore>({ status: 'not_logged_in' });
