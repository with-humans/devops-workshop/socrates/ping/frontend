import { ZonedDateTime, Duration } from '@js-joda/core';
import { render, screen } from '@testing-library/svelte';
import { describe, expect, it, vi } from 'vitest';
import Schedule  from './Schedule.svelte';
import {Event, Schedule as Schedule_} from './domain/ScheduleDomain';
import { tick } from 'svelte';

import { InMemoryProvider } from '../../lib/OpenFeatureInMemoryProvider';
import { initFeatureToggles } from '../../lib/featureToggles';

describe('Schedule', () => {
	it('shows a single item', () => {
		const event: Event = Event.of(
			'World Cafe',
			'Böhmesaal',
			ZonedDateTime.now(),
			Duration.ofMinutes(90)
		);
		const { container } = render(Schedule, { schedule: new Schedule_([event]) });
		expect(container).toHaveTextContent('World Cafe');
	});

	it('shows multiple items', () => {
		const event1: Event = Event.of(
			'World Cafe',
			'Düsseldorf',
			ZonedDateTime.now(),
			Duration.ofMinutes(90)
		);
		const event2: Event = Event.of(
			'Coffee',
			'Frankfurt',
			ZonedDateTime.now(),
			Duration.ofMinutes(90)
		);
		const event3: Event = Event.of(
			'Lunch',
			'Böhmesaal',
			ZonedDateTime.now(),
			Duration.ofMinutes(90)
		);
		const { container } = render(Schedule, { schedule: new Schedule_([event1, event2, event3]) });
		expect(container).toHaveTextContent('World Cafe');
		expect(container).toHaveTextContent('Coffee');
		expect(container).toHaveTextContent('Lunch');
	});

	it('shows the currently running and the next event', () => {
		const currentlyRunningEvent: Event = Event.of(
			'World Cafe',
			'Böhmesaal',
			ZonedDateTime.now().minusMinutes(20),
			Duration.ofMinutes(40)
		);
		const nextEvent: Event = Event.of(
			'Coffee',
			'Böhmesaal',
			ZonedDateTime.now().plusMinutes(30),
			Duration.ofMinutes(10)
		);
		const eventAfterTheNext: Event = Event.of(
			'Lunch',
			'Böhmesaal',
			ZonedDateTime.now().plusMinutes(60),
			Duration.ofMinutes(20)
		);

		const { container } = render(Schedule, {
			schedule: new Schedule_([currentlyRunningEvent, nextEvent, eventAfterTheNext]),
			onClickScheduleEvent: () => {}
		});
		expect(container).toHaveTextContent('World Cafe');
		expect(container).toHaveTextContent('Coffee');
		expect(container).not.toHaveTextContent('Lunch');
	});

	it('shows no previous events as the currently running event and the next event', () => {
		const previousEvent: Event = Event.of(
			'World Cafe',
			'Böhmesaal',
			ZonedDateTime.now().minusMinutes(40),
			Duration.ofMinutes(20)
		);
		const nextEvent: Event = Event.of(
			'Coffee',
			'Böhmesaal',
			ZonedDateTime.now().plusMinutes(30),
			Duration.ofMinutes(1)
		);
		const eventAfterTheNext: Event = Event.of(
			'Lunch',
			'Böhmesaal',
			ZonedDateTime.now().plusMinutes(60),
			Duration.ofMinutes(2)
		);

		const { container } = render(Schedule, {
			schedule: new Schedule_([previousEvent, nextEvent, eventAfterTheNext]),
			onClickScheduleEvent: () => {}
		});
		expect(container).not.toHaveTextContent('World Cafe');
		expect(container).toHaveTextContent('Coffee');
		expect(container).toHaveTextContent('Lunch');
	});

	it("should update the reference 'now' time every interval", async () => {
		const now = ZonedDateTime.now();
		const nowFn = vi.fn().mockImplementation(() => now.plusMinutes(2));
		vi.useFakeTimers();

		const event: Event = Event.of(
			'World Cafe',
			'Böhmesaal',
			now.minusMinutes(40),
			Duration.ofMinutes(60)
		);

		const { container, component } = render(Schedule, {
			now,
			nowFn,
			schedule: new Schedule_([event]),
			updateInterval: 60 * 1000,
			onClickScheduleEvent: () => {}
		});
		const prev = component.$$.ctx[component.$$.props.now as any] as ZonedDateTime;

		expect(container).toHaveTextContent('ends in 20 minutes');

		vi.advanceTimersByTime(120 * 1000);
		await tick();

		const after = component.$$.ctx[component.$$.props.now as any] as ZonedDateTime;

		expect(prev.isBefore(after)).toBeTruthy();
		expect(container).toHaveTextContent('ends in 18 minutes');
	});

	it('allows to select the location if it is a known location', async () => {
		const event1: Event = Event.of(
			'World Cafe',
			'Böhmesaal',
			ZonedDateTime.now(),
			Duration.ofMinutes(90)
		);
		const schedule = new Schedule_([event1]);
		const spy = vi.fn();
		const { container } = render(Schedule, { schedule: new Schedule_([event1]), onClickScheduleEvent: spy });

		screen.getByText('World Cafe').click();

		expect(spy).toHaveBeenCalled();
	});

	it('should not allow to select the location if it is unknown', async () => {
		initFeatureToggles(
			new InMemoryProvider({
				'fix-room-selection-for-unknowns': true
			})
		);
		const event1: Event = Event.of(
			'World Cafe',
			'Somewhere Else',
			ZonedDateTime.now(),
			Duration.ofMinutes(90)
		);
		const schedule = new Schedule_([event1]);
		const spy = vi.fn();
		const { container } = render(Schedule, { schedule: new Schedule_([event1]), onClickScheduleEvent: spy });

		const worldcafe = screen.getByText('World Cafe');
	   	expect(worldcafe.parentElement?.parentElement).toHaveClass('smui-menu-item--non-interactive');
		worldcafe.click();

		expect(spy).not.toHaveBeenCalled();
	});
});
