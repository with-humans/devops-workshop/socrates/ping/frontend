import { ZonedDateTime } from '@js-joda/core';
import { render, screen } from '@testing-library/svelte';
import { describe, expect, it } from 'vitest';
import type { Event } from './Schedule.svelte';
import ScheduleEvent from './ScheduleEvent.svelte';

describe('ScheduleEvent', () => {
	it('shows the time until the event starts', () => {
		const now = ZonedDateTime.now();

		const nextEvent: Event = {
			title: 'Coffee',
			location: 'Böhmesaal',
			startTime: now.plusMinutes(30),
			endTime: now.plusMinutes(40)
		};

		const { container } = render(ScheduleEvent, { event: nextEvent, now });
		expect(container).toHaveTextContent('starts in 30 minutes');
	});

	it('shows the time until the event starts', () => {
		const now = ZonedDateTime.now();

		const nextEvent: Event = {
			title: 'Coffee',
			location: 'Böhmesaal',
			startTime: now.minusMinutes(30),
			endTime: now.plusMinutes(40)
		};

		const { container } = render(ScheduleEvent, { event: nextEvent, now });
		expect(container).toHaveTextContent('ends in 40 minutes');
	});

	it('shows a progress bar if the event is already running', () => {
		const now = ZonedDateTime.now();

		const event: Event = {
			title: 'Coffee',
			location: 'Böhmesaal',
			startTime: now.minusMinutes(10),
			endTime: now.plusMinutes(10)
		};

		render(ScheduleEvent, { event, now });
		expect(screen.getByRole('progressbar')).toHaveAttribute('aria-valuenow', '0.5');
	});

	it('shows a progress bar for an event that just started', () => {
		const now = ZonedDateTime.now();

		const event: Event = {
			title: 'Coffee',
			location: 'Böhmesaal',
			startTime: now,
			endTime: now.plusMinutes(10)
		};

		render(ScheduleEvent, { event, now });
		expect(screen.getByRole('progressbar')).toHaveAttribute('aria-valuenow', '0');
	});

	it('does not show a bar for future events', () => {
		const now = ZonedDateTime.now();

		const event: Event = {
			title: 'Coffee',
			location: 'Böhmesaal',
			startTime: now.plusMinutes(10),
			endTime: now.plusMinutes(20)
		};

		render(ScheduleEvent, { event, now });
		expect(screen.queryByRole('progressbar')).toBeFalsy();
	});
});
