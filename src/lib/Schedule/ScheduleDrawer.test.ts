import { render, screen } from '@testing-library/svelte';
import { describe, expect, it } from 'vitest';
import ScheduleDrawer from './ScheduleDrawer.svelte';

describe('ScheduleDrawer', () => {
	it('links to the schedule app', () => {
		const { container } = render(ScheduleDrawer, { schedule: undefined, open: true });
		expect(screen.getByRole('link', { name: 'Open full schedule' })).toHaveProperty(
			'href',
			'https://scrts.de/socrates2024/index.html'
		);
	});
});
