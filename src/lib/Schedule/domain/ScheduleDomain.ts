import type { Duration, ZonedDateTime } from '@js-joda/core';

export class Event {
	title: string;
	location: string;
	startTime: ZonedDateTime;
	endTime: ZonedDateTime;

	private constructor(
		title: string,
		location: string,
		startTime: ZonedDateTime,
		duration: Duration
	) {
		this.title = title;
		this.location = location;
		this.startTime = startTime;
		this.endTime = startTime.plus(duration);
	}
	static of(title: string, location: string, startTime: ZonedDateTime, duration: Duration): Event {
		return new Event(title, location, startTime, duration);
	}
}
export class Schedule {
	events: Event[];
	constructor(events: Event[]) {
		this.events = events;
	}
	private currentAndFutureEventsByRoom(now: ZonedDateTime): Record<string, Event[]> {
		const events = this.events;
		let currentAndFutureEvents: Event[];
		currentAndFutureEvents = events.filter((event) => event.endTime.isAfter(now));

		let eventsByRoom: Record<string, Event[]>;
		eventsByRoom = currentAndFutureEvents.reduce(
			(r, event) => ({
				...r,
				[event.location]: (r[event.location] || []).concat(event)
			}),
			{} as Record<string, Event[]>
		);
		return eventsByRoom;
	}

	currentAndUpToOneFutureEventPerRoom(now: ZonedDateTime): ([Event] | [Event, Event])[] {
		type EventsByRoom = [Event] | [Event, Event];
		let nextEventsGroupedByRoom: EventsByRoom[] = Object.values(
			this.currentAndFutureEventsByRoom(now)
		)
			.map(
				(eventsInRoom) =>
					eventsInRoom
						.sort((a, b) => a.startTime.compareTo(b.startTime))
						.slice(0, 2) as EventsByRoom
			)
			.filter((eventsByRoom) => eventsByRoom.length > 0);
		return nextEventsGroupedByRoom;
	}
}
