import {
  DateTimeFormatter,
  Duration,
  LocalDateTime,
  ZonedDateTime,
  ZoneId,
} from "@js-joda/core";
import { Event, Schedule } from "../../domain/ScheduleDomain";

const CONCATINATED_KONOPAS_FORMAT = DateTimeFormatter.ofPattern(
  "yyyy-MM-dd HH:mm",
);

export type KonopasEventEntry = {
  id: string;
  title: string;
  date: string;
  time: string;
  mins: string;
  tags: string[];
  loc: string[];
  desc: string;
  people: KonopasEventPersonEntry[];
};

export type KonopasEventPersonEntry = {
  id: string;
  name: string;
};

export default (konopasData: KonopasEventEntry[], zoneId: ZoneId): Schedule => {
  return new Schedule(
    konopasData.flatMap((event) => {
      try {
        return [Event.of(
          event.title,
          event.loc[0],
          LocalDateTime.parse(
            `${event.date} ${event.time}`,
            CONCATINATED_KONOPAS_FORMAT,
          ).atZone(
            zoneId,
          ),
          Duration.ofMinutes(Number(event.mins)),
        )];
      } catch (e) {
        return [];
      }
    }),
  );
};
