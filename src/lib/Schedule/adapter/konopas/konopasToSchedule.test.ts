import { describe, expect, it } from "vitest";
import konopasAcceptanceTestData from "./konopasToSchedule.test.konopas.json";
import konopasToSchedule, { type KonopasEventEntry } from "./konopasToSchedule";
import { Schedule } from "../../domain/ScheduleDomain";
import { ZonedDateTime, ZoneId } from "@js-joda/core";
import "@js-joda/timezone/dist/js-joda-timezone.esm";

describe("konopasToSchedule", () => {
  it.skip("converts konopas to our schedule", () => {
    const expectedSchedule: Schedule = new Schedule([]);

    expect(
      konopasToSchedule(konopasAcceptanceTestData, ZoneId.of("Europe/Berlin")),
    ).toEqual(
      expectedSchedule,
    );
  });

  const template = {
    id: "2002",
    title: "World Cafe",
    date: "2023-08-24",
    time: "19:30",
    mins: "50",
    tags: ["", "", ""],
    loc: ["Böhmesaal"],
    desc: "",
    people: [
      {
        id: "2002",
        name: "all",
      },
    ],
  };
  it("converts the time into a ZonedDateTime based on the timezone provided", () => {
    const inputData = [
      {
        ...template,
        date: "2023-08-24",
        time: "19:30",
      },
    ];

    const schedule = konopasToSchedule(inputData, ZoneId.of("Europe/Berlin"));
    const onlyEvent = schedule.events[0];

    expect(onlyEvent.startTime.toString()).toEqual(
      ZonedDateTime.of(2023, 8, 24, 19, 30, 0, 0, ZoneId.of("Europe/Berlin"))
        .toString(),
    );
  });

  it("provides the correct duration", () => {
    const inputData = [
      {
        ...template,
        date: "2023-08-24",
        time: "19:30",
        mins: "20",
      },
    ];

    const schedule = konopasToSchedule(inputData, ZoneId.of("Europe/Berlin"));
    const onlyEvent = schedule.events[0];

    expect(onlyEvent.startTime.toString()).toEqual(
      ZonedDateTime.of(2023, 8, 24, 19, 30, 0, 0, ZoneId.of("Europe/Berlin"))
        .toString(),
    );
    expect(onlyEvent.endTime.toString()).toEqual(
      ZonedDateTime.of(2023, 8, 24, 19, 50, 0, 0, ZoneId.of("Europe/Berlin"))
        .toString(),
    );
  });

  it("provides the correct location", () => {
    const inputData = [
      {
        ...template,
        loc: ["Pietzmoor Lounge"],
      },
    ];

    const schedule = konopasToSchedule(inputData, ZoneId.of("Europe/Berlin"));
    const onlyEvent = schedule.events[0];

    expect(onlyEvent.location).toEqual("Pietzmoor Lounge");
  });
  it("provides the correct title", () => {
    const inputData = [
      {
        ...template,
        title: "Coffee Break",
      },
    ];

    const schedule = konopasToSchedule(inputData, ZoneId.of("Europe/Berlin"));
    const onlyEvent = schedule.events[0];

    expect(onlyEvent.title).toEqual("Coffee Break");
  });

  it("parses all events provided", () => {
    const inputData = [template, template];

    const schedule = konopasToSchedule(inputData, ZoneId.of("Europe/Berlin"));

    expect(schedule.events[0]).toEqual(schedule.events[1]);
  });

  it("ignores lines with errors", () => {
    const inputData = [template, {
      ...template,
      mins: "hello",
    }, {
      ...template,
      time: "",
    }];

    const schedule = konopasToSchedule(inputData, ZoneId.of("Europe/Berlin"));

    expect(schedule.events).toHaveLength(1);
  });
});
