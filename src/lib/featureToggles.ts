import { writable, type Writable } from 'svelte/store';
import { OpenFeature, ProviderEvents } from '@openfeature/web-sdk';
import type { Provider } from '@openfeature/web-sdk';

const featureToggles: Writable<Record<string, any>> = writable({});

const client = OpenFeature.getClient();

const readFeatureTogglesFromOpenFeature = () => {
	featureToggles.update((old: Record<string, any>) => ({
		...old,
		'enable-user-management': client.getBooleanValue('enable-user-management', false),
		'enable-schedule': client.getBooleanValue('enable-schedule', false),
		'enable-people': client.getBooleanValue('enable-people', false),
		'enable-mood': client.getBooleanValue('enable-mood', false),
		'enable-ping': client.getBooleanValue('enable-ping', false),
		'fix-room-selection-for-unknowns': client.getBooleanValue('fix-room-selection-for-unknowns', false)
	}));
};

let resolve: Function, reject: Function;
OpenFeature.addHandler(ProviderEvents.ConfigurationChanged, (eventDetails) => {
	readFeatureTogglesFromOpenFeature();
});
OpenFeature.addHandler(ProviderEvents.Ready, (eventDetails) => {
	readFeatureTogglesFromOpenFeature();
	resolve();
});
OpenFeature.addHandler(ProviderEvents.Error, (error) => {
	reject(error);
});

const initialReadPromise = new Promise((r, err) => {
	resolve = r;
	reject = err;
});

const initFeatureToggles = (featureProvider: Provider) => {
	OpenFeature.setProvider(featureProvider);
	readFeatureTogglesFromOpenFeature();
	return initialReadPromise;
};

export { featureToggles, initFeatureToggles };
