import type { JsonValue, Provider, ResolutionDetails } from '@openfeature/web-sdk';
import {
	FlagNotFoundError,
	OpenFeatureEventEmitter,
	ProviderEvents,
	StandardResolutionReasons,
	TypeMismatchError
} from '@openfeature/web-sdk';

type FlagValue = boolean | string | number;
export type FlagConfiguration = Record<string, FlagValue>;

export class InMemoryProvider implements Provider {
	public readonly events = new OpenFeatureEventEmitter();
	readonly metadata = {
		name: 'In-Memory Provider'
	} as const;
	private _flagConfiguration: FlagConfiguration;

	constructor(flagConfiguration: FlagConfiguration = {}) {
		this._flagConfiguration = { ...flagConfiguration };
	}

	replaceConfiguration(flagConfiguration: FlagConfiguration) {
		const flagsChanged = Object.entries(flagConfiguration)
			.filter(([key, value]) => this._flagConfiguration[key] !== value)
			.map(([key]) => key);

		this._flagConfiguration = { ...flagConfiguration };
		this.events.emit(ProviderEvents.ConfigurationChanged, { flagsChanged });
	}

	resolveBooleanEvaluation(flagKey: string, defaultValue: boolean): ResolutionDetails<boolean> {
		const flagValue = this.lookupFlagValueOrThrow(flagKey);

		if (typeof flagValue !== 'boolean') {
			throw new TypeMismatchError();
		}

		return staticResolution(flagValue);
	}

	resolveStringEvaluation(flagKey: string, defaultValue: String): ResolutionDetails<string> {
		const flagValue = this.lookupFlagValueOrThrow(flagKey);

		if (typeof flagValue !== 'string') {
			throw new TypeMismatchError();
		}

		return staticResolution(flagValue);
	}

	resolveNumberEvaluation(flagKey: string, defaultValue: number): ResolutionDetails<number> {
		const flagValue = this.lookupFlagValueOrThrow(flagKey);

		if (typeof flagValue !== 'number') {
			throw new TypeMismatchError();
		}

		return staticResolution(flagValue);
	}

	resolveObjectEvaluation<U extends JsonValue>(flagKey: string): ResolutionDetails<U> { }

	private lookupFlagValueOrThrow(flagKey: string): FlagValue {
		if (!(flagKey in this._flagConfiguration)) {
			throw new FlagNotFoundError();
		}
		return this._flagConfiguration[flagKey];
	}
}

function staticResolution<U>(value: U): ResolutionDetails<U> {
	return {
		value,
		reason: StandardResolutionReasons.STATIC
	};
}
