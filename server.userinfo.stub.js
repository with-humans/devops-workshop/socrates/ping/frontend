import express from 'express';

export default () => {
	const app = express();
	app.get('/userinfo', (req, res) => {
		res.status(401).send();
	});
	return app;
};
